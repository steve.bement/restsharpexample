﻿using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using RestSharp;

namespace RestSharpExample
{
    public class SimpleTest
    {
        [Test]
        public async Task ResponseContainsCorrectChartName()
        {
            var client = new RestClient("https://api.coindesk.com/v1/bpi/currentprice.json");
            var request = new RestRequest();
            var responseJson = await client.GetAsync(request);

            dynamic response = JObject.Parse(responseJson.Content);
            Assert.That(response.chartName.Value, Is.EqualTo("Bitcoin"));
            Assert.That(response.bpi.USD.code.Value, Is.EqualTo("USD"));
        }
    }
}
